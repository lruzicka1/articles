\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{tgschola}
\usepackage{hyperref}
\usepackage[english]{babel}

\author{Fedora QA Team}
\date{2022}
\title{Testing RHEL images with openQA}


\begin{document}
\maketitle


openQA is an open source automated testing engine that is able to run an \textsc{iso} file or a \textsc{qcow2} disc image inside a virtual machine and perform various actions there. At Fedora QA, the openQA automated testing is a crucial part of the release validation process and it helps Fedora to keep a maintain a sound level of quality at any time of the development process (Always Ready Rawhide). However, openQA is not limited to test Fedora images. With just a little effort, the existing Fedora settings and scripts can be adapted to run any operating system which could be used for RHEL testing, as well.
 
This article explains what steps need be done in order to run RHEL images inside the Fedora version of openQA.

\section{Prerequisites}

The \textit{openQA} runs best on a \textit{bare metal machine}. Theoretically, it is possible to install the system into a virtual machine and use another layer of virtualization (\textit{nested virtualization}), however this article assumes that openQA will be installed on a bare metal.

As openQA will run as a system service, it will consume some computer resources for itself, but mainly it will need more to run virtual machines where tests are performed. Therefore, your machine should have \textit{enough \textsc{ram}}.\footnote{8GB should be considered a minimum.} The openQA computer runs one of the supported versions of Fedora, both \textit{Workstation} and \textit{Server}. 

\newpage

Fedora OA team provides all necessary openQA packages, as well all the settings, and libraries. The Fedora openQA testing repository also includes all the tests and needles that are used to test Fedora composes and that anyone can use or adapt to suit their needs.

\newpage

\section{openQA installation}

The procedure described in this section will install openQA on your computer and make it ready to use.

\subsection{Installing packages}

The basic installation consists of three packages -- \texttt{openqa}, \texttt{openqa-httpd}, and \texttt{openqa-worker} which are part of Fedora standard repositories, so you can install them using the \textit{dnf} tool.

\begin{enumerate}
	\item Install the openQA packages.
	\begin{verbatim}
	$ sudo dnf install openqa openqa-httpd openqa-worker	
	\end{verbatim}
	\item Tests are started using API commands from the CLI. Install the REST Client for Perl:
	\begin{verbatim}
		$ sudo dnf install perl-REST-Client	
	\end{verbatim}
	\item The way Fedora uses openQA is a little bit different from the SuSE upstream. We have developed several extra scripts to make one's life easier around it. To make it work, install the \texttt{python3-jsonschema} package:
	\begin{verbatim}
		$ sudo dnf python3-jsonschema	
	\end{verbatim}
\end{enumerate}

Now, you have installed all the software needed to run openQA on your machine. The next subsection shows how to configure the \texttt{httpd} server to run the openQA \textit{web UI}.

\subsection{Configure the \texttt{httpd} server}

openQA uses \texttt{httpd} to display a dashboard webpage that not only provides various status information about openQA itself, but also allows to change settings and parameters, investigate failures, explore test results, and restart tests. 

It comes with predefined configuration file templates that will set the \texttt{httpd} to run on \texttt{http://localhost:80}. If you need something else, you are free to edit those configuration files. Then move them in the correct location.

\begin{enumerate}
	\item Go to the \texttt{httpd} config directory where the templates have been created.
	\begin{verbatim}
	$ cd /etc/httpd/conf.d
	\end{verbatim}
	\item Create the \texttt{openqa.conf} file.
	\begin{verbatim}
		$ sudo cp openqa.conf.template openqa.conf
	\end{verbatim}
		\item Create the \texttt{openqa-ssl.conf} file.
	\begin{verbatim}
		$ sudo cp openqa-ssl.conf.template openqa-ssl.conf
	\end{verbatim}
		\item Allow that \texttt{httpd} scripts and modules can connect to the network.
	\begin{verbatim}
	$ sudo setsebool -P httpd_can_network_connect 1
	\end{verbatim}
	\item Start the \texttt{httpd} service if it is not running, or restart if it is.
	\begin{verbatim}
	$ sudo systemctl start httpd
	\end{verbatim}

\end{enumerate}
You have now configured the \texttt{httpd} service to run the default settings for openQA. Before you attempt to open the web UI however, make sure you have made the basic configuration. Check next subsection.



\subsection{Configure the openQA web UI}

The \textit{web UI} settings are stored in the \texttt{/etc/openqa/openqa.ini} file. To make the UI work correctly, we need to make just a minimal set-up:

\begin{enumerate}
	\item Change the \texttt{branding} and the \texttt{download\_domains} in the \texttt{[global]} section to the following values:
	\begin{verbatim}
		[global]
		branding = plain
		download_domains = fedoraproject.org
	\end{verbatim}

	The \texttt{branding} will disable the \textit{SuSE} logo on the welcome page. The \texttt{download\_domains} will only allow to download from the \textit{fedoraproject.org} but if you only strive for running openQA locally, this piece of setting will also not have a huge impact on your local
	instance.
	\item Select the authentication method that you will use to log into the UI. For instances running locally, the most effective method is to use the \texttt{Fake} authentication.
	\begin{verbatim}
		[auth]
		method = Fake
	\end{verbatim}
\end{enumerate}

The openQA web UI now has a basic working configuration. As a next step, install and configure the database to keep openQA settings and test results.

\subsection{Installing and configuring database}

The \texttt{postgresql} database is used to store the settings and test results for openQA. It is again available from the Fedora repositories.

\begin{enumerate}
	\item Install the database server.
	\begin{verbatim}
		$ sudo dnf install postgresql-server
	\end{verbatim}
	\item Run the init scripts to set up the database.
	\begin{verbatim}
	$ sudo postgresql-setup --initdb
	\end{verbatim}
\end{enumerate}

You have now completed the necessary set up and you can run openQA for the first time.

\subsection{Running openQA}

openQA consists of several system services that need to be up and running. Make sure all the following services are working correctly.\footnote{Use \texttt{sudo systemctl status <service>} to see if it is running correctly.} If not, (re)start them\footnote{Use \texttt{sudo systemctl start <service>} or \texttt{sudo systemctl restart <service>}.}:

\begin{itemize}
	\item postgresql
	\item httpd
	\item openqa-gru
	\item openqa-scheduler
	\item openqa-websockets
	\item openqa-webui
\end{itemize}

If you want that openQA starts automatically with every boot, you can \texttt{enable} them using the \texttt{systemctl} command.

Now, openQA should \textit{be ready to work with}, so you can open a browser and point it to \url{http://localhost}. If everything went fine, you will find yourself at the openQA welcome page.

Before you can start tests, you need to create an API key.

\subsection{Creating the API key}

The openQA web UI does not currently allow to schedule or run tests, it only lets users rerun tests that have been scheduled by an external CLI command (see \nameref{run-tests}) communicating over the openQA API. Therefore, you need to create an API key to allow the CLI command manipulate the openQA instance:

\begin{enumerate}
	\item Go to the openQA web UI at \url{http://localhost}.
	\item Click \textbf{Login} to log into the Web UI.\footnote{If you have set up the fake authentication, the user \textit{Demo} should be now logged in.}
	\item Click \textbf{Manage API Keys} to open the API key management page.
	\item In the \textit{New API Key} section, click on \textbf{Create} to create a new API key.\footnote{If you do not wish the API to expire in some time, unclick the \textbf{Expiration} check button.}
	\item The new API key has been created and you can now see that it consists of a \textit{Key} and a \textit{Secret}. 
	\item Edit the \texttt{/etc/openqa/client.conf} file and add the key and secret that you have created in the previous step.
	\begin{verbatim}
		[localhost]
		key = 123456789
		secret = 987654321
	\end{verbatim}
\end{enumerate}

You are now ready to schedule tests using the openQA API.

\subsection{Adding openQA workers}
	
	You could have scheduled some jobs already, but they will not be completed without the workers. You need to add \textit{at least one} worker to perform the jobs:
	
	\begin{verbatim}
		$ sudo systemctl start openqa-worker@1
	\end{verbatim}

	Mind the number \texttt{1} in the command. This represents the first worker. If you need more workers, you can start them independently, replacing the number in the above command with the consequent numbers (\texttt{2}, \texttt{3}, \texttt{4}, etc.).
	
	Note, that running more workers will allow to run more jobs at the same time, but it will significantly increase the memory demand on your computer.
	
	At this point, we have openQA set up and running, however if you browse through the UI and search for \textit{Machines}, \textit{Medium types}, \textit{Test suites}, or \textit{Job groups}, you will not find any. In the next section, we will see how to populate it with predefined Fedora defaults.

\newpage

\section{Creating a minimal distribution to run tests.}
\label{minimal-distri}

To be able to run tests, we need to create a minimal distribution (so called \texttt{distri}) to set up \textit{machines} that will be used to run the tests, \textit{scenarios} to cover the single test jobs, and \textit{testsuites} to define test specific details.

\subsection{Creating the directory structure}

The minimal distribution consists of a specific directory structure that you need to create in the openQA's system directory. To create the structure:

\begin{enumerate}
	\item Go to \texttt{/var/lib/openqa/tests} directory.
	\item Create the distribution working directory with a dedicated name, i.e. \texttt{rhel}:  \vspace{15pt} \\ \texttt{\$ sudo mkdir rhel}
	\item Go inside the newly created directory:  \vspace{15pt} \\ \texttt{\$ cd rhel}
	\item Create \texttt{tests} and  \texttt{needles} directories to store your tests and needles:  \vspace{15pt} \\ \texttt{\$ sudo mkdir \{tests,needles\} }
	\item Go back to the distribution directory: \vspace{15pt} \\ \texttt{\$ cd /var/lib/openqa/tests/rhel}.
	\item Create a main openQA file: \vspace{15pt} \\ \texttt{\$ sudo touch main.pm}
	\item Change ownership to \texttt{geekotest}, so that it can be accessed by openQA: \vspace{15pt} \\ \texttt{\$ sudo chown -R geekotest:geekotest *}
\end{enumerate}

Alternatively, you can clone the minimal distribution from the openQA repository\footnote{\url{https://github.com/os-autoinst/os-autoinst-distri-example}} into the distribution directory, i.e. \texttt{rhel}, and set the ownership to it as described in step 7.

\subsection{Creating the \texttt{main.pm} content}

If you have cloned the minimal distribution from the openQA repository, you can skip this step. Otherwise make sure, that the you have typed the following content into the \texttt{main.pm} file.

\begin{verbatim}
	use strict;
	use testapi;
	use autotest;
	
	autotest::loadtest 'tests/boot.pm';
	
	1;
\end{verbatim}

Notice the line beginning with \texttt{autotest::loadtest}. This is how tests that should be run inside the openQA are loaded. Currently, the \texttt{main.pm} script will only load the \texttt{boot.pm} script that should be located in the \texttt{tests} directory. If you only need to run a couple of tests, you could load them this way, but if you plan on adding more tests later, you should use a more convenient way to do so. 

One option is to use a \textit{test loading} variable that you can use within a \textit{test suite}, see \nameref{create-test-suite}, and load the scripts through this variable.

In Fedora, we use the \texttt{ENTRYPOINT} variable and the following definition inside the \texttt{main.pm} file:

\begin{verbatim}
	if (get_var("ENTRYPOINT")) {
	    my @tests = split(/ /, get_var("ENTRYPOINT"));
	        foreach (@tests) {
	            autotest::loadtest "tests/$_";
	        }
	}
\end{verbatim}

\enlargethispage{-30pt}

This code will check for the existence of the \texttt{ENTRYPOINT} variable and if it exists, it will read its content, split it on a white space, and run all of the scripts one after another. In the \textit{test suite} description, you could load one or more scripts using the following line:

\begin{verbatim}
	ENTRYPOINT=boot.pm login.pm firefox.pm
\end{verbatim}

\subsection{Creating a testing machine}
\label{create-machine}

You will need to have at least one machine to be able to run tests. This \textit{machine} is a representation of a virtual machine type. Basically, you need to give it a \texttt{name} and the \texttt{backend}\footnote{To see what kind of backends can be used, see \url{https://github.com/os-autoinst/os-autoinst/tree/master/backend}.} which will be used to run it. 

Also, you can specify various settings to create the virtual machine using specified \textit{variables}\footnote{More on variables can be seen at \url{https://github.com/os-autoinst/os-autoinst/blob/master/doc/backend_vars.asciidoc}.}

To create a new machine:

\begin{enumerate}
	\item Open the openQA webUI and log into it.
	\item From the menu, select \textit{Machines}. If you already have some machines, they will be listed in this view together with their settings. 
	\item Scroll down to the bottom of the page and click on \textit{New machine}. 
	\item Fill in the \texttt{name}, the \texttt{backend}, and the \texttt{variables}.
	\item Click on the \textit{floppy symbol} to save it.
\end{enumerate}

The following can be used to set up a fully working, \textit{qemu} based machine using the \textit{x86\_64} architecture as used by the Fedora openQA. 

\begin{enumerate}
	\item The \textit{name} is \texttt{64bit}.
	\item The \textit{backend} is \texttt{qemu}.
	\item The settings are:
	\begin{verbatim}
		ARCH_BASE_MACHINE=64bit
		PART_TABLE_TYPE=mbr
		QEMUCPU=Nehalem
		QEMUCPUS=2
		QEMURAM=3072
		QEMUVGA=virtio
		QEMU_MAX_MIGRATION_TIME=480
		QEMU_VIRTIO_RNG=1
		WORKER_CLASS=qemu_x86_64
		XRES=1024
		YRES=768
	\end{verbatim}
\end{enumerate}

You can safely use the above to create a similar machine run tests inside your distribution.



\subsection{Creating a test suite}
\label{create-test-suite}
A test suite is a representation of a single test or of a group of tests. Each test suite has got a \texttt{name} and the \texttt{settings}. You can also provide a \texttt{description} to make it easier to see the purpose of the test suite, for instance.

To create a new test suite:

\begin{enumerate}
	\item Log into the webUI.
	\item From the menu, select \textit{Test suites}.
	\item Scroll down to the bottom of the page and click on \textit{New test suite.}
	\item Fill in the \texttt{name}, the \texttt{settings}, and the \texttt{description} (if you wish so).
	\item Click on the \textit{floppy symbol} to save it.
\end{enumerate}

To set up features for the test suite, you can use system variables, as mentioned above, or you can create your own variables and values. Both types of variables will then be available to the running tests through the \texttt{get\_var("VARIABLE")} function.

The following is a very simple example of a test suite that uses a \texttt{qcow2} image to boot and perform one simple test.

\begin{enumerate}
	\item The \textit{name} is \texttt{boot\_to\_gui}.
	\item The \textit{settings} are:
	\begin{verbatim}
		BOOTFROM=c 
		HDD_1=rhel-image.qcow2
		ENTRYPOINT=boot.pm
	\end{verbatim}
\end{enumerate}
The \texttt{BOOTFROM} variable is used to tell the worker to try booting from the hard drive first. The \texttt{HDD\_1} variable tells the worker which image to load, see the \nameref{copying-rhel-image} section. The \texttt{ENTRYPOINT} variable loads the corresponding test script to be run on that machine. 

Note, that \texttt{ENTRYPOINT} is not a system variable but a custom one that is used by Fedora openQA. You can choose a different variable, such as \texttt{LOAD}, or you can choose an entirely different way to load tests to suit your needs, see \nameref{minimal-distri}. 

Use the above to create a basic test suite that you can register with openQA to get the basic test running.

\subsection{Creating a job group}

You can imagine the \textit{job group} as a high level container for \textit{products} and \textit{scenarios}. A \textit{product} could be a test flavour, such as \textit{RHEL 8 Workstation}, \textit{RHEL 9 Server} or any other combination that meets your needs, while \textit a {scenario} would be \textit{a set of test suites} running on that product.

\subsubsection{Create a new job group}

To create a new job group:

\begin{enumerate}
	\item Log into the webUI.
	\item Open the webUI menu.
	\item Click on \textbf{Job groups}.
	\item Click on the \textbf{Add new job group}  button in the upper right corner.\footnote{Looks like a blue plus.}
	\item Click \textbf{Save changes} to save the job group.
\end{enumerate}

The following definition will create a job group that will run on an \texttt{x86\_64} machine, it will have one product and one scenario will be placed in it. 

You can use it as a minimal template to create more detailed structures.

\begin{itemize}
	\item \textbf{The \texttt{defaults} section} allows you to set up several variables to control the entire job group. These can be overridden in later steps, if needs be, otherwise they will be valid for all products and test scenarios.
	
	\begin{verbatim}
		defaults:
		  x86_64:
		    machine: 64bit
		    priority: 50  
	\end{verbatim}
	
	The above introduces the \texttt{x86\_64} architecture that uses the \texttt{64bit} machine (see \nameref{create-machine}) and priority of \texttt{50} to run all tests in this group.
	
	\item \textbf{The \texttt{products} section}
	
	This section defines various products in this job group. The following sets a product named \texttt{rhel-server-x86\_64} using the \texttt{rhel} distri and flavor and it runs for all versions.
	
	\begin{verbatim}
		products:
		  rhel-server-x86_64:
		    distri: rhel
		    flavor: rhel
		    version: "*"
	\end{verbatim}

	\item \textbf{The \texttt{scenarios} section}
	
	This section defines various test scenarios that run inside the predefined product. The scenarios can contain one or more test suites. 
	
	The following will invoke a test suite named \texttt{boot\_to\_gui} to run inside the \texttt{rhel-server-x86\_64} product on a \texttt{x86\_64} architecture.
	
	\begin{verbatim}
		scenarios:
		  x86_64:
		    rhel-server-x86_64:
		    - boot_to_gui
	\end{verbatim}
	
\end{itemize}

Now, when you put all this together, you can save it as a basic job group that will let you start with testing.

\begin{verbatim}
defaults:
  x86_64:
    machine: 64bit
    priority: 50
products:
  rhel-server-x86_64:
    distri: rhel
    flavor: rhel
    version: "*"
scenarios:
  x86_64:
    rhel-server-x86_64:
    - boot_to_gui

\end{verbatim}

\newpage

\section{Loading Fedora defaults into openQA}

If you wanted to cooperate on development Fedora tests and needles, or you would just want to test Fedora with openQA the way we do it on \url{openqa.fedoraproject.org}, you can clone the Fedora distribution repository instead and set it up accordingly.

The entire collection of settings, libraries, tests, and test needles is provided as a Pagure repository\footnote{\url{https://pagure.io/fedora-qa/os-autoinst-distri-fedora.git}} and maintained by the Fedora QA team. You can always rely on it to have the most up-to-date tests and settings that are used in real and everyday Fedora testing, so they will most probably work fine.

The openQA's main application directory is located at \texttt{/var/lib/openqa} where everything is stored. In order to load the Fedora collection into your openQA instance:

\begin{enumerate}
	\item Go to the \texttt{tests} directory.
	\begin{verbatim}
		$ cd /var/lib/openqa/tests
	\end{verbatim}
	\item Clone the \texttt{os-autoinst-distri-fedora} repository as \texttt{fedora}.
	\begin{verbatim}
		$ sudo git clone \ 
		https://pagure.io/fedora-qa/os-autoinst-distri-fedora.git \
		fedora
	\end{verbatim}
	\item Change the ownership of the entire \texttt{fedora} directory to \texttt{geekotest} so that openQA can use it without any limitations.
	\begin{verbatim}
		$ sudo chown -R geekotest:geekotest fedora
	\end{verbatim}
	\item Enter the \texttt{fedora} directory. From now on, let us refer to this directory as the \textit{repository}.
	\begin{verbatim}
		$ cd fedora
	\end{verbatim}
	\item There is a file \texttt{templates.fif.json} located in this directory, that holds all the information about machines, products, test suites, etc., and also a \texttt{fifloader.py} script that loads the templates into openQA, creating all necessary structures for you. To load the templates run the following command:
	\begin{verbatim}
	$ sudo ./fifloader.py -c -l templates.fif.json
	\end{verbatim}
\end{enumerate}

At this point, your openQA instance is able to perform all test suites that are tested in Fedora production as a part of release validation testing.

\newpage

\section{Copying the RHEL image into openQA}
\label{copying-rhel-image}

openQA can use an installation file (\textsc{iso}) and you can develop test scripts that will navigate through the installation and finally they will leave an installed disk image (asset) in the \texttt{qcow2} format which you can use to load and perform further testing. This is mostly how Fedora tests its composes because testing installations is a crucial part of the release validation process.

However, if installations and system preparations are not what you need to test, it might be better to provide openQA with a preinstalled disk image directly which you can author to suit all your needs. This will save you a lot of time because you can only focus on tests you really need without having to maintain the installation scripts.

This approach will be described in the following text.

\subsection{Creating a dedicated directory to store images}

openQA, by default, is installed in the \texttt{/var/lib/openqa} folder where it keeps all the assets, tests and results. The subdirectory, where \textsc{iso} files and disk images are stored, is the \texttt{/var/lib/openqa/share/factory} directory.\footnote{This directory can also be accessed through a symlink \texttt{factory} in the openQA installation directory, i.e \texttt{/var/lib/openqa/factory}.} 

This directory should contain two directories:
\begin{itemize}
	\item \texttt{iso} with the \textsc{iso} files created during installation tests, and
	\item \texttt{hdd} with \textsc{qcow2} disk images.
\end{itemize}

After you have freshly installed openQA, these two directories \textit{are not created} for you and you \textit{need to create them} manually.

\begin{verbatim}
 	$ sudo mkdir /var/lib/openqa/factory/{iso,hdd}
\end{verbatim}

\subsection{Moving in the pre-created qcow2 image}
\label{place-image}

To make your image accessible by openQA, copy (or move) it in the \texttt{hdd} directory and make sure it is readable for the user \texttt{geekotest}.\footnote{You can make it world readable if it suits your needs better.} For the sake of this document, let us assume that the image name is \texttt{rhel-image.qcow2}.

Now, the image is ready to be used withing openQA, however placing it in the correct directory \textit{will not} make openQA aware of it automatically. You need to specify that in the configuration files as described further on.

\newpage

%\section{Setting up openQA to run the image}
%
%Traditionally, openQA uses \textsc{yaml} files to set up machines, products and test jobs, which is rather complicated and requires certain knowledge of its structure. Also, \textsc{yaml} files are difficult to maintain and it is easy to make a mistake which than prevents you from updating the settings correctly.
%
%In Fedora openQA, however, everything can be set up easily through the so called \textit{templates}, i.e. in the \texttt{templates.fif.json} file, located in the repository directory. We are using this approach to describe a creation of a new and unique \textit{test product} that will be able to run tests connected with that particular image. 
%
%To do so basically requires several edits the \texttt{templates.fif.json} file.
%
%\subsection{Creating a new product}
%\label{create-product}
%
%In order to run your tests independently, i.e. not as part of the usual Fedora tests, you need to create a new \textit{product} (job group) that will act as a container for all tests associated with your image.
%
%To add a new product:
%
%\begin{enumerate}
%	\item Open the \texttt{templates.fif.json} file in a text editor.\footnote{If you work inside the openQA installation directory, do not forget to obtain permissions to save that file.} 
%	\item Find the \texttt{fedora-universal-x86\_64-*} subsection inside the \texttt{Products} section.
%	\item Use it as a model to create a new product of your own, that will run on the \texttt{x86\_64} architecture.\footnote{If a different product suits you better, you may freely use any of the existing products as a model for the new product of yours.}
%	\item Copy it into the  \texttt{Products} section and change the features accordingly, for example:
%	
%	\newpage
%	
%	\begin{verbatim}
%		"fedora-rhel-x86_64-*": {
%		    "arch": "x86_64",
%		    "distri": "fedora",
%		    "flavor": "rhel",
%		    "settings": {
%			        "TEST_TARGET": "ISO"
%		    },
%		    "version": "*"
%		},
%	\end{verbatim}
%
%	This definition will create a product called \texttt{fedora-rhel-x86\_64-*} using the \texttt{x86\_64} architecture on the \texttt{fedora} distribution\footnote{The \texttt{fedora} distri should not be changed.}, with the product shortened name \texttt{rhel} that will run for all versions.
%\end{enumerate}
%
%\subsection{Using a machine}
%\label{using-machine}
%
%A \textit{machine} is a virtual \textsc{qemu} machine that will be created and used to run your images or \textsc{iso} files. In the \texttt{Machines} section of the \texttt{templates.fif.json} file, you can see machines that are used when testing Fedora, for example an \texttt{x86\_64} machine with the name \textit{64bit} and other, rather self-explanatory, settings:
%
%\begin{verbatim}
%"64bit": {
%    "backend": "qemu",
%    "settings": {
%        "ARCH_BASE_MACHINE": "64bit",
%        "PART_TABLE_TYPE": "mbr",
%        "QEMUCPU": "Nehalem",
%        "QEMUCPUS": "2",
%        "QEMURAM": "3072",
%        "QEMUVGA": "virtio",
%        "QEMU_MAX_MIGRATION_TIME": "480",
%        "QEMU_VIRTIO_RNG": "1",
%        "XRES": "1024",
%        "YRES": "768",
%        "WORKER_CLASS": "qemu_x86_64"
%    }
%},
%\end{verbatim}
%
%If these default settings are fine with you, use one of the pre-defined machines. If you need something else, you can either edit one of the current machines, or you can copy the subsection, give it another name, and change the settings to suit your needs better.
%
%\subsection{Creating a new profile}
%
%A \textit{profile} is a combination of a \textit{product} (see \nameref{create-product}) and a~\textit{machine} which will be used to run that product. 
%
%To create a new profile, you can copy an existing one in the \texttt{Profiles} section and change its settings, for instance:
%
%\begin{verbatim}
%"fedora-rhel-x86_64-*-64bit": {
%    "machine": "64bit",
%    "product": "fedora-rhel-x86_64-*"
%},
%\end{verbatim}
%\begin{flushleft}
%This will create a profile that runs tests in the scope of \linebreak \texttt{fedora-rhel-x86\_64-*} product using the \texttt{64bit} machine (see \nameref{using-machine}).
%\end{flushleft}
%
%
%\subsection{Creating a new test suite}
%
%The \textit{test suite} is a collection of one or more tests that run in the scope of a product. When assigning test jobs to openQA, the CLI command (see \nameref{run-tests}) allows to start tests for the whole product, which means that all test suites associated with that product will be started, ordered according to a given priority where tests with lower priority number will start sooner than those with a higher priority number. 
%
%To create a new test suite, you might want to copy one of the existing suites and change the settings, for instance the following will create a test suite with the name \textit{rhel-testing}:
%
%\begin{verbatim}
%"rhel-testing": {
%    "profiles": {
%        "fedora-rhel-x86_64-*-64bit": 20
%    },
%    "settings": {
%        "BOOTFROM": "c",
%        "HDD_1": "rhel-9.qcow2",
%        "ENTRYPOINT": "<desktop_login>"
%    }
%},
%\end{verbatim}
%
%The variables used in this test suite stand for
%
%\begin{description}
%	\item[profiles] In which profiles this test suite will run. You should use your newly created profile to make sure it will only start when you run that profile. The number \texttt{20} is the priority number for this test suite in that profile. The lower the number is, the higher priority it will get. The tests with lowest priority number run first.
%	\item[settings] contain various settings for the test suite. You can pass values to your test variables here. 
%	\item[BOOTFROM] tells openQA from which drive it shall boot. If you only use one disk image, make sure the value is set to \texttt{c}.
%	\item[HDD\_1] takes the name of the \textsc{qcow2} image that you have placed in the \texttt{hdd} directory (see \nameref{place-image}).
%	\item[ENTRYPOINT] is used to run one or more tests provided here as a list of values divided by a white space. The test name is the name of the test script without the suffix, relative to the  \texttt{tests} directory in the repository. For example, the mentioned script \texttt{desktop\_login}, would have a corresponding test file in \texttt{desktop\_login.pm}. Also, if the test is placed somewhere else, you need to provide a relative path from the repository directory.
%\end{description}
%
%The tests may also be invoked by using the \texttt{POSTINSTALL} or alternatively \texttt{POSTINSTALL\_PATH} variables. Check the \texttt{VARIABLES.md} file in
%the repository directory for variables that you can use in the test suites' definitions and their meaning.
%
%Add more test suites, if you wish so.
%
%When all the above steps have been made, it is now possible to run test suites in that particular product.

\section{Running the tests within a product}
\label{run-tests}

As mentioned before, the tests in openQA cannot be started from withing the web UI. They are started with the \texttt{openqa-cli} command.\footnote{Check the openQA user guide for more info on how to invoke tests at \url{http://open.qa/docs/\#usersguide}.} This command takes several obligatory variables (as arguments) that always need to be provided. Optionally, you can also provide more if you have not specified them in the test suite definition and you need them. 

The minimal command to start tests within a certain product looks like the following example:

\begin{verbatim}
	sudo openqa-cli api -X POST isos DISTRI=<distribution> \ 
	VERSION=<version> FLAVOR=<product> ARCH=<architecture> \ 
	BUILD=<build>
\end{verbatim}

To run the \texttt{rhel} product on an \texttt{x86\_64} architecture, you should run something like

\begin{verbatim}
	sudo openqa-cli api -X POST isos DISTRI=rhel VERSION=9 \
	FLAVOR=rhel ARCH=x86_64 BUILD=20221007
\end{verbatim}

At this point, your tests run inside the openQA and you can watch their progress and results in the web UI.

\newpage

\section{What to do next?}

Quite a number of options have now appeared in front of you. You can, for example, 

\begin{itemize}
	\item check the openQA documentation at \url{http://open.qa/documentation/}
	\item study the existing Fedora test scripts at \texttt{/var/lib/openqa/tests/fedora/tests}.
	\item study the Fedora libraries at \texttt{/var/lib/openqa/tests/fedora/lib} and \texttt{/var/lib/openqa/tests/fedora/main.pm}
	\item check the existing Fedora needles at \texttt{/var/lib/openqa/tests/fedora/needles}
	\item check the \textbf{Needly} application to create and modify needles at \linebreak \url{https://github.com/lruzicka/needly}
\end{itemize}

\newpage

\section{Conclusion}

This document shows how anyone can use openQA to run their image inside openQA and use it to run various tests in it. If you have completed all steps described here, you will have a minimal working distribution  \texttt{rhel} that will be able to run the basic test suite as part as the \texttt{rhel} flavor. 
\end{document}